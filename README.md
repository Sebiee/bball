# BBall #

### What's this? ###

* This is a game developed to be used with the board MCB-STR9
* This application is a modification added to the executable "HIDClient" that comes with Keil MDK

### How do I get set up? ###

* Compile
* Connect the board to PC with a cable
* Enter the executable, select your board and play!

### Who do I talk to? ###

* Repo owner
* pamparau.sebastian@gmail.com