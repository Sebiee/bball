#pragma once

#include <atomic>
#include <thread>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include <iostream>
#include <conio.h>
#include <string>
#include <random>
#include <time.h>

#define WIDTH 110
#define HEIGHT 20
#define BILA 'o'
#define BILASUS 167
#define OBS1 219
#define OBS2 '\262'
#define SOL 177
#define WIN 60000 //40
#define LEVEL1 7500 //5
#define LEVEL2 15000 //10
#define LEVEL3 30000 //20
#define LEVEL4 45000 //30

using namespace std;

void consoleInit();
void center(string);
void space(int);
void joc();