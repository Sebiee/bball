#include "StdAfx.h"
#include "functii.h"

using namespace std;

bool pSpace = FALSE, start = FALSE, BOOM = FALSE, gameOn = FALSE;
int lost = 0, jCounter = -1, obsCounter = -1, uObs = -1, jObs = -1;

void ShowConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}

void SetConsoleColour(WORD &Attributes, WORD Colour)
{
	CONSOLE_SCREEN_BUFFER_INFO Info;
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hStdout, &Info);
	Attributes = Info.wAttributes;
	SetConsoleTextAttribute(hStdout, Colour);
}

void ResetConsoleColour(WORD Attributes)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Attributes);
}

void gotoxy(int column, int line)
{
	COORD coord;
	coord.X = column;
	coord.Y = line;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE),
		coord
	);
}

/*void readCin() THIS IS FOR THE PC VERSION
{
	while (lost == 0)
	{
		if (getch() == 32)
		{
			pSpace = TRUE;
		}
	}
}*/

void consoleInit()
{
	AllocConsole();
	freopen("conin$", "r", stdin);
	freopen("conout$", "w", stdout);
	freopen("conout$", "w", stderr);
	ShowConsoleCursor(FALSE);
	HANDLE hConOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	//CONSOLE SIZE
	SMALL_RECT r;
	COORD      c;
	if (!GetConsoleScreenBufferInfo(hConOut, &csbi)) {
		std::cout << "Jocul nu a putut fi initializat.";
		_getch();
		exit(0);
	}
	c.X = WIDTH;
	c.Y = HEIGHT;
	SetConsoleScreenBufferSize(hConOut, c);
	r.Left = r.Top = 0;
	r.Right = c.X - 1;
	r.Bottom = c.Y - 1;
	SetConsoleWindowInfo(hConOut, TRUE, &r);
	WORD A = 0;
	while (1) {
		pSpace = FALSE;
		lost = 0;
		jCounter = -1;
		obsCounter = -1;
		system("cls");
		SetConsoleColour(A, 10);
		center("BBall v0.1");
		ResetConsoleColour(A);
		cout << "\n\n";
		gotoxy(0, 5);
		center("Apasati butonul S2 de pe placuta MCB-STR9 pentru a incepe jocul");
		gotoxy(0, 6);
		center("In timpul jocului, apasati butonul S2 pentru a sari si butonul S3 pentru a distruge toate obstacolele.");
		start = FALSE;
		while (start == FALSE);
		gotoxy(0, 5);
		space(WIDTH);
		gotoxy(0, 6);
		space(WIDTH);
		joc();
		gameOn = FALSE;
		while (gameOn == FALSE);
	}
	return;
}

void center(string s)
{
	space(WIDTH / 2 - s.size() / 2);
	cout << s;
}

void space(int i)
{
	for (int j = 0; j < i; j++)
		cout << " ";
}

void gameOver()
{
	WORD A = 0;
	SetConsoleColour(A, 79);
	gotoxy(0, 4);
	center(" .--.                       .--.               ");
	space(31);
	cout << "\n";
	center(": .--'                     : ,. :              ");
	space(31);
	cout << "\n";
	center(": : _ .--. ,-.,-.,-..--.   : :: .-..-..--..--. ");
	space(31);
	cout << "\n";
	center(": :; ' .; ;: ,. ,. ' '_.'  : :; : `; ' '_.: ..'");
	space(31);
	cout << "\n";
	center("`.__.`.__,_:_;:_;:_`.__.'  `.__.`.__.`.__.:_;  ");
	space(31);
	cout << "\n";
	center("                                               ");
	space(31);
	cout << "\n";
	center("                                               ");
	space(31);
	cout << "\n";
	ResetConsoleColour(A);
	center("Apasati butonul S2 si butonul S3 de pe placuta MCB-STR9.");
	space(27);
	//http://patorjk.com/software/taag/#p=display&h=3&v=3&f=Fuzzy&t=Game%20Over
}

void winner()
{
	WORD A = 0;
	SetConsoleColour(A, 47);
	gotoxy(0, 4);
	center(".-..-.           .-.   .-.         .-.");
	space(36);
	cout << "\n";
	center(": :: :           : :.-.: :         : :");
	space(36);
	cout << "\n";
	center("`.  ..--..-..-.  : :: :: :.--.,-.,-: :");
	space(36);
	cout << "\n";
	center(" .' ' .; : :; :  : `' `' ' .; : ,. :_;");
	space(36);
	cout << "\n";
	center(":_,'`.__.`.__.'   `.,`.,'`.__.:_;:_:_;");
	space(36);
	cout << "\n";
	center("                                      ");
	space(36);
	cout << "\n";
	center("                                      ");
	space(36);
	cout << "\n";
	ResetConsoleColour(A);
	center("Apasati butonul S2 si butonul S3 de pe placuta MCB-STR9.");
	space(27);
	//http://patorjk.com/software/taag/#p=display&h=3&v=3&f=Fuzzy&t=You%20won!%0AGod%20bless%20you!
}

void joc()
{
	WORD A = 0;
	//std::thread cinThread(readCin); PC VERSION
	srand(time(0));
	char v[3][WIDTH];
	int i, difficulty = 1500, c=0, BOMBS = 4;
	for (i = 0; i < WIDTH; i++) {
		v[2][i] = ' ';
		v[1][i] = ' ';
		v[0][i] = SOL;
	}
	v[1][3] = BILA;
	//cout << v[2] << v[1] << v[0];
	while (lost == 0) {
		gotoxy(2, 1);
		cout << "LEVEL: " << difficulty / 1500 << " - ";
		if (difficulty >= LEVEL4) {
			SetConsoleColour(A, 12);
			cout << "Foarte repede    ";
			ResetConsoleColour(A);
		}
		else if (difficulty >= LEVEL3) {
			SetConsoleColour(A, 4);
			cout << "Repede         ";
			ResetConsoleColour(A);
		}
		else if (difficulty >= LEVEL2) {
			SetConsoleColour(A, 15);
			cout << "Repejor         ";
			ResetConsoleColour(A);
		}
		else if (difficulty >= LEVEL1) {
			SetConsoleColour(A, 7);
			cout << "Incet        ";
			ResetConsoleColour(A);
		}
		else {
			SetConsoleColour(A, 8);
			cout << "Foarte Incet       ";
			ResetConsoleColour(A);
		}
		gotoxy(2, 2);
		cout << "To next level:    \b\b\b" << 70 - c;
		gotoxy(2, 3);
		cout << "MAX Level: " << WIN / 1500 << " | " << "Mai puteti folosi ";
		if (BOMBS > 0)
			SetConsoleColour(A, 10);
		else
			SetConsoleColour(A, 12);
		cout << BOMBS;
		ResetConsoleColour(A);
		cout << " bombe.";
		c++;
		//cout << 'a';
		if (c > 70) {
			c = 0;
			difficulty += 1500;
		}
		if (difficulty >= WIN) {
			lost = 3;
		}
		/*
		* DESENARE JOC
		*/
		for (i = 0; i < WIDTH; i++) {
			gotoxy(i, 9);
			if (v[2][i] == BILA) 
				SetConsoleColour(A, 10);
			else
				SetConsoleColour(A, 12);
			cout << v[2][i];
			ResetConsoleColour(A);
			gotoxy(i, 10);
			if (v[1][i] == BILA)
				SetConsoleColour(A, 10);
			else
				SetConsoleColour(A, 12);
			cout << v[1][i];
			gotoxy(i, 11);
			ResetConsoleColour(A);
			SetConsoleColour(A, 242);
			cout << v[0][i];
			ResetConsoleColour(A);
		}
		/*
		* MUTARE BILA
		*/
		if (jCounter > -1) {
			jCounter--;
		}
		if (jCounter == 0 && pSpace == TRUE) {
			v[2][3] = ' ';
			v[1][3] = BILA;
			pSpace = FALSE;
		}
		if (pSpace == TRUE) {
			if (v[2][4] != ' ') {
				v[1][4] = 167;
				lost = 1; //pierdere sus;
			}
			else if(jCounter == -1){
				v[2][3] = BILA;
				v[1][3] = ' ';
				jCounter = 3;
			}
		}
		if (v[2][3] == BILA) {
			if (v[2][4] != ' ') {
				lost = 1;
				v[2][3] = BILA; // BILA Speciala pierdere sus
			}
		} else {
			if (v[1][4] != ' ') {
				lost = 2;
				v[1][3] = BILA; // BILA Speciala pierdere dreapta
			}
		}
		v[0][0] = v[0][1];
		v[0][1] = v[0][2];
		v[0][2] = v[0][3];
		v[0][3] = v[0][4];
		//
		v[1][0] = v[1][1];
		v[1][1] = v[1][2];
		if (v[1][2] == (char)OBS1)
			v[1][2] = ' ';
		if (v[1][3] != BILA) {
			v[1][2] = v[1][3];
			v[1][3] = v[1][4];
		}
		//
		v[2][0] = v[2][1];
		v[2][1] = v[2][2];
		if (v[2][2] == (char)OBS1)
			v[2][2] = ' ';
		if (v[2][3] != BILA) {
			v[2][2] = v[2][3];
			v[2][3] = v[2][4];
		}
		for (i = 4; i < WIDTH - 1; i++) {
			v[2][i] = v[2][i + 1];
			v[1][i] = v[1][i + 1];
			v[0][i] = v[0][i + 1];
		}
		/*
		* CREARE OBSTACOLE
		*/
		if (((difficulty + (rand() % (int)(WIN))) > LEVEL3+2500) && uObs < (1 + (rand() % (int)5))) { //OBSTACOL SUS
			if (obsCounter == -1) {
				v[2][WIDTH - 1] = OBS1;
				obsCounter = 4;
				uObs++;
				if (jObs >(1 + (rand() % (int)(5))))
					jObs--;
			} else {
				v[2][WIDTH - 1] = ' ';
				obsCounter--;
			}
			v[1][WIDTH - 1] = ' ';
		} else if ((difficulty + (rand() % (int)(WIN))) > LEVEL3+2500 && jObs < (1 + (rand() % (int)5))) { //OBSTACOL JOS
			if (obsCounter == -1) {
				v[1][WIDTH - 1] = OBS1;
				obsCounter = 4;
				jObs++;
				if (uObs >(1 + (rand() % (int)(5))))
					uObs--;
			}
			else {
				v[1][WIDTH - 1] = ' ';
				obsCounter--;
			}
			v[2][WIDTH - 1] = ' ';
		} else {
			v[1][WIDTH - 1] = ' ';
			v[2][WIDTH - 1] = ' ';
			uObs = jObs = -1;
		}
		v[0][WIDTH - 1] = SOL;
		if (difficulty >= LEVEL4)
			Sleep(35);
		else if (difficulty >= LEVEL3)
			Sleep(40);
		else if (difficulty >= LEVEL2)
			Sleep(50);
		else if (difficulty >= LEVEL1)
			Sleep(60);
		else
			Sleep(65);
		if (BOOM == TRUE && BOMBS>0){
				BOMBS--;
				BOOM = FALSE;
				for (i = 0; i < WIDTH; i++) {
					v[2][i] = ' ';
					v[1][i] = ' ';
					v[0][i] = SOL;
				}
				v[1][3] = BILA;
		}
	}
	if (lost == 3)
		winner();
	else
		gameOver();
	//cinThread.join(); PC VERSION
	return;
}